//
//  AppConstants.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import UIKit

enum EndPoints: String {
    case schoolsList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    case SATScoresList = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
}

struct AppConstants {
        
    
    //MARK: -Global Variable
    static var allSatScores = [SATScore]()
    
    //MARK: - Helper Methods
    static func showAlert(withTitle title: String, Message message: String, controller: UIViewController) {
        let ac = UIAlertController(title: title, message: message, preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: title, style: .default))
        controller.present(ac, animated: true)
    }
    
}
