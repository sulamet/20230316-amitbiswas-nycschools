//
//  SchoolInformationViewController.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import UIKit

class SchoolInformationViewController: UIViewController {

    //MARK: - View Model
    var viewModel: SchoolAdditionalInformationViewModel?
    
    
    //MARK: - Outlets
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var schoolLocationLabel: UILabel!
    
    @IBOutlet weak var schoolOverview: UILabel!
    
    @IBOutlet weak var readingAvgScore: UILabel!
    @IBOutlet weak var mathsAvgScoreLabel: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        self.updateData()
        self.viewModelBinding()
        self.fetchSATScores()
        self.activityIndicator.stopAnimating()
        
    }
}


//MARK: - View Model Binding
extension SchoolInformationViewController {
    
    private func viewModelBinding() {
        self.viewModel?.completionHandler = { output in
            switch output {
            
            case .updateData:
                DispatchQueue.main.async {
                    self.updateData()
                }
                break
                
            case .hideLoader:
                DispatchQueue.main.async {
                    self.activityIndicator.stopAnimating()
                }
                break
                
            case .showLoader:
                DispatchQueue.main.async {
                    self.activityIndicator.startAnimating()
                }
                break
                
            case .showError(let message):
                DispatchQueue.main.async {
                    AppConstants.showAlert(withTitle: "Error", Message: message, controller: self)
                }
                break
                
            }
        }
    }
    
}


//MARK: - Private Methods
extension SchoolInformationViewController {
    
    private func updateData() {
        self.schoolNameLabel.text = viewModel?.school.schoolName
        self.schoolLocationLabel.text = viewModel?.school.location
        self.schoolOverview.text = viewModel?.school.overviewParagraph
        
        self.mathsAvgScoreLabel.text = viewModel?.satScore?.mathScore ?? ""
        self.readingAvgScore.text = viewModel?.satScore?.readingScore ?? ""
        self.writingAvgScore.text = viewModel?.satScore?.writingScore ?? ""
    }
    
}

//MARK: - Private Methods
extension SchoolInformationViewController {
    
    private func fetchSATScores() {
        self.viewModel?.fetchSATScores()
    }

}
