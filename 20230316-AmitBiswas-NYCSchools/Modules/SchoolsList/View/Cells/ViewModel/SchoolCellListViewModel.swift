//
//  SchoolCellListViewModel.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation


struct SchoolCellListViewModel {
    
    let schoolName: String
    let schoolCity: String
    let cellIndex: Int
    
    
    init(cellIndex: Int, schoolName: String, schoolCity: String) {
        self.cellIndex = cellIndex
        self.schoolName = schoolName
        self.schoolCity = schoolCity
    }
    
}
