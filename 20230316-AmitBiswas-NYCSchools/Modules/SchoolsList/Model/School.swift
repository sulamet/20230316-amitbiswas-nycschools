//
//  School.swift
//  20200704-AmitBiswas-NYCSchools
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation

struct School: Codable {
    
    var dbn: String?
    var schoolName: String?
    var overviewParagraph: String?
    var location: String?
    var phoneNumber: String?
    var faxNumber: String?
    var schoolEmail: String?
    var website: String?
    var finalGrades: String?
    var totalStudents: String?
    var city: String?
    var councilDistrict: String?
    var stateCode: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn = "dbn"
        case schoolName = "school_name"
        case overviewParagraph = "overview_paragraph"
        case phoneNumber = "phone_number"
        case city = "city"
        case location = "location"
        case website = "website"
        case faxNumber = "fax_number"
        case schoolEmail = "school_email"
        case finalGrades = "finalgrades"
        case totalStudents = "total_students"
        case councilDistrict = "council_district"
        case stateCode = "state_code"
    }
}
