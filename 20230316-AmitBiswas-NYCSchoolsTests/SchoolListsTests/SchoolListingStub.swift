//
//  SchoolListingStub.swift
//  20200704-AmitBiswas-NYCSchoolsTests
//
//  Created by Amit Biswas on 3/16/23.
//

import Foundation


let schoolListStub = """
[
{
"dbn": "02M260",
"school_name": "Clinton School Writers & Artists, M.S. 260",
"overview_paragraph": "Students who are prepared for college must have an education that encourages them to take risks as they produce and perform. Our college preparatory curriculum develops writers and has built a tight-knit community. Our school develops students who can think analytically and write creatively. Our arts programming builds on our 25 years of experience in visual, performing arts and music on a middle school level. We partner with New Audience and the Whitney Museum as cultural partners. We are a International Baccalaureate (IB) candidate school that offers opportunities to take college courses at neighboring universities.",
"location": "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)",
"phone_number": "212-524-4360",
"fax_number": "212-524-4365",
"school_email": "admissions@theclintonschool.net",
"website": "www.theclintonschool.net",
"finalgrades": "6-12",
"total_students": "376",
"city": "Manhattan",
"state_code": "NY",
"council_district": "2"
},
{
"dbn": "21K728",
"school_name": "Liberation Diploma Plus High School",
"overview_paragraph": "The mission of Liberation Diploma Plus High School, in partnership with CAMBA, is to develop the student academically, socially, and emotionally. We will equip students with the skills needed to evaluate their options so that they can make informed and appropriate choices and create personal goals for success. Our year-round model (trimesters plus summer school) provides students the opportunity to gain credits and attain required graduation competencies at an accelerated rate. Our partners offer all students career preparation and college exposure. Students have the opportunity to earn college credit(s). In addition to fulfilling New York City graduation requirements, students are required to complete a portfolio to receive a high school diploma.",
"location": "2865 West 19th Street, Brooklyn, NY 11224 (40.576976, -73.985413)",
"phone_number": "718-946-6812",
"fax_number": "718-946-6825",
"school_email": "scaraway@schools.nyc.gov",
"website": "schools.nyc.gov/schoolportals/21/K728",
"finalgrades": "6-12",
"total_students": "376",
"city": "Brooklyn",
"state_code": "NY",
"council_district": "47"
}
]
"""
