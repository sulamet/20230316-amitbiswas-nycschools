//
//  SchoolsListingTests.swift
//  20200704-AmitBiswas-NYCSchoolsTests
//
//  Created by Amit Biswas on 3/16/23.
//

import XCTest
@testable import _0200704_AmitBiswas_NYCSchools


class SchoolsListingTests: XCTestCase {

    var schoolListingViewModel: SchoolsListViewModel?
        
    override func setUpWithError() throws {
        let data = schoolListStub.data(using: .utf8)!
        do {
            
            let schoolList = try JSONDecoder().decode([School].self, from: data)
            self.schoolListingViewModel = SchoolsListViewModel(schoolsList: schoolList)
            
        } catch let jsonErr {
            print("Error serializing json:", jsonErr)
        }
    }

    func testSchoolListNotEmpty() {
        XCTAssertEqual(schoolListingViewModel?.rows, 2)
    }

    func testFirstListItemCityIsManhattan() {
        XCTAssertEqual(schoolListingViewModel?.getSchoolFor(index: 0).city, "Manhattan")
    }
    
    func testFirstListItemStatusCodeIsNY() {
        XCTAssertEqual(schoolListingViewModel?.getSchoolFor(index: 0).stateCode,"NY")
    }

}
