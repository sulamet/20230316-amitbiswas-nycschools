//
//  SchoolDetailTests.swift
//  20200704-AmitBiswas-NYCSchoolsTests
//
//  Created by Amit Biswas on 3/16/23.
//

import XCTest

@testable import _0200704_AmitBiswas_NYCSchools

class SchoolDetailTests: XCTestCase {

    var schoolDetailViewModel: SchoolAdditionalInformationViewModel?
        
    override func setUpWithError() throws {
        let satData = SATScoreStub.data(using: .utf8)!
        let schoolData = schoolListStub.data(using: .utf8)!
        do {
            
            let schoolList = try JSONDecoder().decode([School].self, from: schoolData)
            let satScore = try JSONDecoder().decode(SATScore.self, from: satData)
            
            if let firstSchool = schoolList.first {
                self.schoolDetailViewModel = SchoolAdditionalInformationViewModel(school: firstSchool)
                self.schoolDetailViewModel?.satScore = satScore
                
            }
            
        } catch let jsonErr {
            print("Error serializing json:", jsonErr)
        }
    }

    func testMathSATAverageScoreIs423() {
        XCTAssertEqual(schoolDetailViewModel?.satScore?.mathScore ?? nil, "423")
    }
    
    func testWritingSATAverageScoreIs366() {
        XCTAssertEqual(schoolDetailViewModel?.satScore?.writingScore ?? nil, "366")
    }
    
    func testReadingSATAverageScoreIs383() {
        XCTAssertEqual(schoolDetailViewModel?.satScore?.readingScore ?? nil, "383")
    }
    
}
